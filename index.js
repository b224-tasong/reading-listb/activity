// Part 2: Simple Server - Node.js

/*
	1. Create an index.js file inside of the activity folder. 
	2.Import the http module using the required directive. Create a variable port and assign it with the value of 8000.
	3. Create a server using the createServer method that will listen in to the port provided above.
	4. Console log in the terminal a message when the server is successfully running.
	5. Create a condition that when the login route, the register route, the homepage and the usersProfile route is accessed, it will print a message specific to the page accessed.
	6. Access each routes to test if it’s working as intended. Save a screenshot for each test.
	7. Create a condition for any other routes that will return an error message.

*/
// Code here:

let http = require("http");

let port = 8000;


http.createServer((request, response) =>{

	if (request.url == "/" &&  request.method =="GET") {

		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to Website Homepage');
	}else if (request.url == "/login" &&  request.method =="GET") {

		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to Login Page');
	}else if (request.url == "/usersProfile" &&  request.method =="GET") {

		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to your User Profile');
	}else if (request.url == "/registerPage" &&  request.method =="GET") {

		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to Registration Page');
	}else{
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Page not available!');
	}



}).listen(port);



console.log(`Server is running at localhost: ${port}`);